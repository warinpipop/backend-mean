const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/CrudDB',
{ useNewUrlParser: true})

mongoose.connection.on('connected',(err)=>{
    if(!err)
    console.log('MongoDB connection succeeded.');
    else
    console.log('Error in DB connection : ' + JSON.stringify(err, undefined, 2));
})


process.on("SIGINT", function(){
    mongoose.connection.close(function(){
        console.log('mongoose close')
        process.exit(0);
    })
})
module.exports = mongoose;