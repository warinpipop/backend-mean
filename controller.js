const express = require('express')
var router = express.Router();


const masterModel = require('./masterModel')
router.get('/', (req, res) => {
    masterModel.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Employees :' + JSON.stringify(err, undefined, 2)); }
    });
});


router.post('/', (req,res)=>{
    var master = new masterModel({
        date: req.body.date,
        firstname: req.body.firstname,
        lastname: req.body.lastname
    })
    master.save((err,doc)=>{
        if(!err){
            res.send(doc)
        }else{
            console.log('Error in master add:' + JSON.stringify(err, undefined, 2));
        }
    })
})


router.delete('/:id', (req,res)=>{
    masterModel.findByIdAndRemove(req.params.id, (err,doc)=>{
        if(!err)
        res.send(doc)
        else
        console.log('Error in Employee Delete :' + JSON.stringify(err, undefined, 2));
    })
})


router.put('/:id', (req, res) => {
    console.log('update')
        var master = {
            date: req.body.date,
            firstname: req.body.firstname,
            lastname: req.body.lastname
        }
        masterModel.findByIdAndUpdate(req.params.id, 
            { $set: master }, 
            { new: true }, 
            function(err, doc) {
        if (!err)  
        res.json(doc) 
        else 
         console.log('Error in marter Update :' + JSON.stringify(err, undefined, 2)); 
    });
});
module.exports = router;