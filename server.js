const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const datacontrollor = require('./controller')
app.use(function (req, res, next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'content-type, x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    next();
})
require('./db')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.listen(3000, () => console.log('Server started at port : 3000'));

app.use('/', datacontrollor)