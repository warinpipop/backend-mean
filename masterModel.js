const mongoose = require('mongoose')

var masterField = mongoose.model('masterField',{
    date: Date,
    firstname: {type: String, required:true},
    lastname: {type: String, required: true}
})

module.exports = masterField;

